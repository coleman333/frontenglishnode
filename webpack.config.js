module.exports = {
    entry: [
        './app/bower_components/angular',
        './app/bower_components/angular-animate',
        './app/bower_components/angular-route',
        './app/bower_components/angular-cookies',
        './app/uibootstrap.js',
        './app/app.js'],
    output: {
        path: './app/build',
        filename: 'app.js'
    },
    watch: true
};
