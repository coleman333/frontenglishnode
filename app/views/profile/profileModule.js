var profileCtrl = require('./profileCtrl');

angular.module('myApp.profile', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'views/profile/profile.html',
            controller: 'profileCtrl',
            resolve: ['$http', '$location', function ($http, $location) {
                $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                    .catch(function (res) {
                        $location.path('/');
                    })
            }]
        });
    }])

    .controller('profileCtrl', profileCtrl);

module.exports = 'myApp.profile';