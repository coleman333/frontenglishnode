module.exports = ['$scope', '$http', '$location', function ($scope, $http, $location) {
    $http.get('http://localhost:3000/api/profile', {withCredentials: true})
        .then(function (res) {
            $scope.user = res.data;

        }, function (res) {
            alert(res.data.message);
            $location.path('/')
        });
}];
