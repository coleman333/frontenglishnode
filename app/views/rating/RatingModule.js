'use strict';
var ratingCtrl = require('./RatingCtrl');

angular.module('myApp.rating', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/rating', {
            templateUrl: 'views/rating/Rating.html',
            controller: 'RatingCtrl'
        });
    }])

    .controller('RatingCtrl', ratingCtrl);

module.exports = 'myApp.rating';