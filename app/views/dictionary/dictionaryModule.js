var dictionaryCtrl = require('./dictionaryCtrl');
//название модуля     название модудей от которых наследуется этот
angular.module('myApp.dictionary', ['ngRoute'])
    //названия сервисов зависимостей    переменная через которую мы подключаем сервис
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/dictionary', //google.com/vocabulary
            {
                templateUrl: 'views/dictionary/dictionary.html',
                controller: 'dictionaryCtrl',
                resolve: ['$http', '$location', function ($http, $location) {
                    $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                        .catch(function (res) {
                            $location.path('/');
                        })
                }]
            })
    }])
    //создание контроллера
    .controller("dictionaryCtrl", dictionaryCtrl)

    .controller('AddNewWordModalCtrl', ['$scope', '$uibModalInstance', 'word',
        function ($scope, $uibModalInstance, word) {
            console.log(word);
            $scope.ok = function () {
                $uibModalInstance.close({name: 'alex', age: 28});
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

module.exports = 'myApp.dictionary';