module.exports = [
    '$scope',
    '$http',
    '$uibModal',
    function ($scope, $http, $uibModal) {
        $http.get('http://localhost:3000/api/vocabulary/dictionary/', {withCredentials: true})
            .then(function (res) {
                $scope.allWords = res.data.pictures;


            }, function (res) {
                alert(res.data);
            });
        $scope.addNewWord = function () {
            var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'myModalAddNewWord.html',
                    controller: 'AddNewWordModalCtrl',
                    size: 'xs',
                    resolve: {
                        word: function () {
                            return $scope.newWord;
                        }
                    }
                }
            );

            modalInstance.result.then(function (data) {
                console.dir(data);
            }, function (message) {
                console.log(message);
            });
        };
    }];
