'use strict';
var RegistrationCtrl = require('./RegistrationCtrl');

angular.module('myApp.registration', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/registration', {
            templateUrl: 'views/RegPage/Registration.html',
            controller: 'RegistrationCtrl'
        });
    }])

    .controller('RegistrationCtrl', RegistrationCtrl);

module.exports = 'myApp.registration';