module.exports = [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.theme = 'theme';
        $http.get('http://localhost:3000/api/vocabulary/sentence_constructor/' + $scope.theme, {withCredentials: true})
            .then(function (res) {
                $scope.sentence = res.data;
                $scope.sentence.answer = new Array($scope.sentence.mainSentence.length);
            }, function (res) {
                alert(res.data);
            });
        var currentIndex = 0;
        var isNeedCheck = false;
        $scope.click = function (word, index) {
            $scope.sentence.answer[currentIndex] = word;
            $scope.sentence.shaffledSentence.splice(index, 1);
            currentIndex++;
            if (currentIndex == $scope.sentence.answer.length) {

                //$scope.colors = [];
                //for (var i = 0; i < $scope.sentence.answer.length; i++) {
                //    if ($scope.sentence.answer[i] == $scope.sentence.mainSentence[i]) {
                //        $scope.colors.push('btn-success');
                //    } else {
                //        $scope.colors.push('btn-danger');
                //    }
                //}

                /*for (var i = 0; i < $scope.sentence.answer.length; i++) {
                 if ($scope.sentence.answer[i] == $scope.sentence.mainSentence[i]) {
                 $scope.color = 'success';
                 }
                 else {
                 $scope.sentence.answer[i].color = 'danger';
                 }

                 }*/
                isNeedCheck = true;
            }
        };

        $scope.clickBack = function (word, index) {
            if (!word) {
                return;
            }
            $scope.sentence.shaffledSentence.push(word);
            $scope.sentence.answer = $scope.sentence.answer.slice(0, index).concat($scope.sentence.answer.slice(index + 1));
            $scope.sentence.answer.push(undefined);
            currentIndex--;
            isNeedCheck = false;
            //$scope.colors = [];
        };

        $scope.color = function (item, index) {
            if (isNeedCheck) {
                return 'btn-' + ($scope.sentence.mainSentence[index] == item ? 'success' : 'danger');
            }
            return '';
        }
    }];
