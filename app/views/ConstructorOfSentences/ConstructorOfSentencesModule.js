var ConstructorOfSentencesCtrl = require('./ConstructorOfSentencesCtrl');
//название модуля     название модудей от которых наследуется этот
angular.module('myApp.ConstructorOfSentences', ['ngRoute'])
    //названия сервисов зависимостей    переменная через которую мы подключаем сервис
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/constructor-of-sentences', //google.com/vocabulary
            {
                templateUrl: 'views/ConstructorOfSentences/ConstructorOfSentences.html',
                controller: 'ConstructorOfSentencesCtrl',
                resolve: ['$http', '$location', function ($http, $location) {
                    $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                        .catch(function (res) {
                            $location.path('/');
                        })
                }]
            })
    }])
    //создание контроллера
    .controller("ConstructorOfSentencesCtrl", ConstructorOfSentencesCtrl);

module.exports = 'myApp.ConstructorOfSentences';