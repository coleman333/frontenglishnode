module.exports = ['$scope', '$http', function ($scope, $http) {
    $scope.theme = 'theme';
    $http.get('http://localhost:3000/api/vocabulary/word-constructor/' + $scope.theme, {withCredentials: true})
        .then(function (res) {
            $scope.word = res.data.mainWord;
            $scope.word.shaffled = res.data.shaffledWord;
            $scope.word.answer = new Array($scope.word.shaffled.length);
            $scope.word.successLeters = [];
            $scope.word.currentIndex = 0;
            $scope.word.progress = function (isRound) {
                var result = $scope.word.successLeters.length / $scope.word.answer.length * 100;
                return isRound ? Math.round(result) : result;
            };
        }, function (res) {
            console.log(res.data);
        });

    $scope.click = function (letter, index) {
        if ($scope.word.englishWord[$scope.word.currentIndex] == letter) {
            $scope.word.answer[$scope.word.currentIndex] = letter;
            $scope.word.successLeters.push(index);
            $scope.word.currentIndex++;
        }
    };


    $scope.isSuccessLetter = function (index) {
        return $scope.word.successLeters.some(function (elem) {
            return elem === index;
        });
    };
    
    $scope.next=function () {
        $http.get('http://localhost:3000/api/vocabulary/word-constructor/' + $scope.theme ,{withCredentials:true})
            .then(function(res){
                
            })
    }
}];
