var ConstructorOfWordsCtrl = require('./ConstructorOfWordsCtrl');
//название модуля     название модудей от которых наследуется этот
angular.module('myApp.ConstructorOfWords', ['ngRoute'])
    //названия сервисов зависимостей    переменная через которую мы подключаем сервис
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/constructor-of-words', //google.com/vocabulary
            {
                templateUrl: 'views/ConstructorOfWords/ConstructorOfWords.html',
                controller: 'ConstructorOfWordsCtrl',
                resolve: ['$http', '$location', function ($http, $location) {
                    $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                        .catch(function (res) {
                            $location.path('/');
                        })
                }]
            })
    }])
    //создание контроллера
    .controller("ConstructorOfWordsCtrl", ConstructorOfWordsCtrl);

module.exports = 'myApp.ConstructorOfWords';
