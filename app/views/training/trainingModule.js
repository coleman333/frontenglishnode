var trainingCtrl = require('./trainingCtrl');
//название модуля     название модудей от которых наследуется этот
angular.module('myApp.training', ['ngRoute'])
    //названия сервисов зависимостей    переменная через которую мы подключаем сервис
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/training', //google.com/vocabulary
            {
                templateUrl: 'views/training/training.html',
                controller: 'trainingCtrl',
                resolve: ['$http', '$location', function ($http, $location) {
                    $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                        .catch(function (res) {
                            $location.path('/');
                        })
                }]
            })
    }])
    //создание контроллера
    .controller("trainingCtrl", trainingCtrl);

module.exports = 'myApp.training';