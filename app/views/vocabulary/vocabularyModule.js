var vocabularyCtrl = require('./vocabularyCtrl');
           //название модуля     название модудей от которых наследуется этот
angular.module('myApp.vocabulary', ['ngRoute'])
    //названия сервисов зависимостей    переменная через которую мы подключаем сервис
    .config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/training/vocabulary/:lang', //google.com/vocabulary
            {
                templateUrl: 'views/vocabulary/vocabulary.html',
                controller: 'vocabularyCtrl',
                resolve: ['$http', '$location', function ($http, $location) {
                    $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                        .catch(function (res) {
                            $location.path('/');
                        })
                }]
            })
    }])
    //создание контроллера
    .controller("vocabularyCtrl", vocabularyCtrl);

module.exports = 'myApp.vocabulary';