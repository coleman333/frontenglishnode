module.exports = [
    '$scope',
    '$http',
    '$routeParams',
    function ($scope, $http, $routeParams) {
        $scope.theme = 'theme';
        //$scope.colorr = 'btn-info';

        next();
        $scope.convertWordPair = function (word, isMain) {
            if (!word) {
                return '';
            }
            if ($routeParams.lang === 'en') {
                return isMain ? word.englishWord : word.rusWord;
            } else {
                return isMain ? word.rusWord : word.englishWord;
            }
        };

        $scope.next = next;

        function next() {
            $http.get('http://localhost:3000/api/vocabulary/' + $scope.theme + '/' + $routeParams.lang, {withCredentials: true})
                .then(function (res) {
                        console.dir(res.data);
                        $scope.mainWord = res.data.mainWord;
                        $scope.anotherWords = res.data.anotherWords;
                    },
                    function (res) {
                        console.dir(res.data);
                    });
        }


        $scope.isCorrect = function (item, mainWord) {
            if (item.englishWord != mainWord.englishWord) {
                item.colorr = 'btn-danger';
            }
            $scope.btnNext = true;
        }
    }
];