'use strict';

require('./views/RegPage/RegistrationModule');
require('./views/home/homeModule');
require('./views/profile/profileModule');
require('./views/training/trainingModule');
require('./views/vocabulary/vocabularyModule');
require('./views/audioTraining/audioTrainigModule');
require('./views/ConstructorOfWords/ConstructorOfWordsModule');
require('./views/ConstructorOfSentences/ConstructorOfSentencesModule');
require('./views/dictionary/dictionaryModule');
require('./views/rating/RatingModule');

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngAnimate',
    'ngCookies',
    'ui.bootstrap',
    'myApp.registration',
    'myApp.home',
    'myApp.profile',
    'myApp.training',
    'myApp.vocabulary',
    'myApp.audioTrainig',
    'myApp.ConstructorOfWords',
    'myApp.ConstructorOfSentences',
    'myApp.dictionary',
    'myApp.rating'
])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/'});
    }])

    .controller('TopBarCtrl', ['$scope', '$location', '$uibModal', '$http',
        function ($scope, $location, $uibModal, $http) {

            $scope.isSignIn = function () {
                //return !!$cookies.get('userAuthId');
            };

            /*
             $scope.isRegistration = function () {
             return $location.path().indexOf('/registration') == 0;
             };*/

            $scope.logout = function () {
                //$cookies.remove('userAuthId');
                $http.get('http://localhost:3000/auth/logout', {withCredentials: true})
                    .then(function () {
                        $scope.isAuth = false;
                        $location.path('/');
                    });

            };
            isAuth();
            function isAuth() {
                $http.get('http://localhost:3000/auth/isauth', {withCredentials: true})
                    .then(function (res) {
                        $scope.isAuth = true;
                    }, function (res) {
                        $scope.isAuth = false;
                    });
            }

            $scope.signIn = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'SignInModal.html',
                    controller: 'SignInCtrl',
                    size: 'md'
                });

                modalInstance.result.then(function (user) {
                    $http.post('http://localhost:3000/auth/login', user, {withCredentials: true})
                        .then(function (res) {
                            //$cookies.put('userAuthId', res.data.id);
                            isAuth();
                            $location.path('/profile');
                        }, function (res) {
                            alert(res.data.message);
                        });

                }, function (data) {
                    console.log(data);
                });
            }

        }])

    .controller('SignInCtrl', ['$scope', '$uibModalInstance',
        function ($scope, $uibModalInstance) {
            $scope.submit = function (user) {
                $uibModalInstance.close(user);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);
